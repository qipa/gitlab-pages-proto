# Protobuf specifications and client libraries for gitlab-pages

** Under development **

This repository contains the Protobuf definitions for the admin
service of gitlab-pages. The admin service is used to manage sites
hosted by gitlab-pages.

Run `make` from the root of the repository to regenerate the client
libraries after updating .proto files.

See
[developers.google.com](https://developers.google.com/protocol-buffers/docs/proto3)
for documentation of the 'proto3' Protocol buffer specification
language.

## Contributing

The CI at https://gitlab.com/gitlab-org/gitlab-pages-proto regenerates the
client libraries to guard against the mistake of updating the .proto
files but not the client libraries. This check uses `git diff` to look
for changes. Some of the code in the Go client libraries is sensitive
to implementation details of the Go standard library (specifically,
the output of gzip). **Use the same Go version as .gitlab-ci.yml (Go
1.8)** when generating new client libraries for a merge request.

[DCO + License](CONTRIBUTING.md)

### Build process

After you change or add a .proto file you need to re-generate the Go
and Ruby libraries before committing your change.

```
# Re-generate Go and Ruby libraries
make generate
```

## Release

This will tag and release the gitlab-pages-proto library, including
pushing the gem to rubygems.org

```
make release version=X.Y.Z
```


## How to manually push the gem

If the release script fails the gem may not be pushed. This is how you can do that after the fact:

```shell
# Use a sub-shell to limit scope of 'set -e'
(
  set -e

  # Replace X.Y.Z with the version you are pushing
  GEM_VERSION=X.Y.Z

  git checkout v$GEM_VERSION
  gem build gitlab-pages-proto.gemspec
  gem push gitlab-pages-proto-$GEM_VERSION.gem
)
```
